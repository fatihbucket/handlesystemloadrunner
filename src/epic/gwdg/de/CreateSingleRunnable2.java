package epic.gwdg.de;


import net.handle.hdllib.HandleRecordsBulk;
import net.handle.hdllib.SingleHandleRecord;

public class CreateSingleRunnable2 implements Runnable {

	private HandleMethods handleMethods;
	private HandleRecordsBulk recordsBulk;
	
	public CreateSingleRunnable2(HandleRecordsBulk recordsBulk, HandleMethods handleMethods){
		this.recordsBulk = recordsBulk;
		this.handleMethods = handleMethods;

		
	}

	
	
	@Override
	public void run() {
		 long tid = Thread.currentThread().getId();
		for(int i = 0; i< recordsBulk.size();i++){
			SingleHandleRecord handleRecord = recordsBulk.getHandleRecord(i);
			int resCode = handleMethods.createHandle(handleRecord.handle, handleRecord.values);
			System.out.println("INTERTAL TID="+tid+" CREATE HANDLE (MIX) RC = "+resCode);
		}
		
		
			
	}

}
