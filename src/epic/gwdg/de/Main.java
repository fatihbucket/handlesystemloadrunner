package epic.gwdg.de;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import net.handle.hdllib.Encoder;
import net.handle.hdllib.HandleRecordForBulk;
import net.handle.hdllib.HandleRecordsBulk;
import net.handle.hdllib.Util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Main {

	
	 
	public static void main(String[] args) throws InterruptedException {
		
	
        String config_filename = args[0];
        Config.createInstance(config_filename);
        String runType = Config.getInstance().runtype;
		System.out.println("RUN TYPE = "+runType);
		System.out.println("Reading available Handle Lists ...");
		HandleLists.getInstance();
		
		if (runType.equals("compare")){
			
			WorkloadCompare workloadCompare = new WorkloadCompare();
		}
		else if (runType.equals("mixture"))
		{

			WorkloadMixture workloadMixture = new WorkloadMixture();
		}
		else if (runType.equals("concurrency"))
		{

			WorkloadIncreasingConcurrency workloadIncreasingConcurrency = new WorkloadIncreasingConcurrency();
		}
		
 }

}
