package epic.gwdg.de;


import java.util.UUID;

import net.handle.hdllib.HandleRecordsBulk;
import net.handle.hdllib.SingleHandleRecord;
import net.handle.hdllib.Util;

public class CreateSingleRunnable implements Runnable {


	private HandleMethods handleMethods;	
	private long endTime;
	private long interval;
	
	
	public CreateSingleRunnable(HandleMethods handleMethods, long endTime, long interval){
		this.handleMethods = handleMethods;
		this.endTime = endTime;
		this.interval = interval;
	}
	
	
	@Override
	public void run() {
		
		
		while( (System.currentTimeMillis()<endTime) ){
			
		  long tid = Thread.currentThread().getId();
			
		  SingleHandleRecord handleRecord =	HandleDataSets.getInstance().getHandleRecord();
		
		  UUID suffix = UUID.randomUUID();
		  String handle = Config.getInstance().prefix+"/"+suffix+":s";
		  int resCode = handleMethods.createHandle(Util.encodeString(handle), handleRecord.values);
		  System.out.println("TID="+tid+" CREATE HANDLE RC = "+resCode);
		  try {
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				System.out.println("ERROR in TID="+tid+"CREATE HANDLE");
				e.printStackTrace();
			} 
			
		}
		
	}
	

	
	

}
