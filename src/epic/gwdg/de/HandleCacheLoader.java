package epic.gwdg.de;

import java.util.List;

import net.handle.hdllib.HandleValue;
import net.handle.hdllib.SingleHandleRecord;
import net.handle.hdllib.Util;

import com.google.common.cache.CacheLoader;

public class HandleCacheLoader extends CacheLoader<String,SingleHandleRecord> {
	
	private HandleStorage handleStorage;
	
	public HandleCacheLoader(){
		
		if (Config.getInstance().sqlite == 1){
			handleStorage = new SQLiteMethods();
			handleStorage.establishConnection();
		}
		else{
			handleStorage = new SQLMethods();
			handleStorage.establishConnection();
		}
		
		
	}

	@Override
	public SingleHandleRecord load(String handle) throws Exception {
		
		
		List<HandleValue> list_values = handleStorage.getHandleRecord(handle);
		HandleValue[] values = new HandleValue[list_values.size()];
		values = list_values.toArray(values);
		SingleHandleRecord singleHandleRecord = new SingleHandleRecord(Util.encodeString(handle),values);
		return singleHandleRecord;
		
	}

}
