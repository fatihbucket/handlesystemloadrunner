package epic.gwdg.de;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class HandleLists {
	
	
	private static HandleLists instance = null;
	
	private ArrayList<String> knwonHandles;   
	
	private ArrayList<String> hdlList_1734;   // 8 Values
	private ArrayList<String> hdlList_1779;   // 8 Values
	private ArrayList<String> hdlList_1735;   // 3 Values
	private ArrayList<String> hdlList_203C;   // 3 Values
	private ArrayList<String> hdlList_097C;   // 3 Values
	private ArrayList<String> hdlList_001M;   // 3 Values
	private HandleStorage handleStorage = null;
	
	private Random r_inst = new Random();
	private Random r_hdl = new Random();
	private Random k_hdl = new Random();
	
	
	private Map<Integer,ArrayList<String>> listMap = new HashMap<Integer,ArrayList<String>>();
	
	public static HandleLists getInstance() {
	      if(instance == null) {
	          instance = new HandleLists();
	       }
	       return instance;
	 }
	
	
	
	private HandleLists(){
		
		System.out.println("SQLITE OR MYSQL?");
		if (Config.getInstance().sqlite == 1){
			System.out.println("    ITS SQLITE!");
			this.handleStorage = new SQLiteMethods();
			this.handleStorage.establishConnection();
		}
		else{
			System.out.println("    ITS MYSQL!");
			this.handleStorage = new SQLMethods();
			this.handleStorage.establishConnection();
		}
		
		hdlList_1734 = handleStorage.getHandles("1734");
		listMap.put(0,hdlList_1734);
		hdlList_1779 = handleStorage.getHandles("1779");
		listMap.put(4,hdlList_1779);
		hdlList_1735 = handleStorage.getHandles("1735");
		listMap.put(2,hdlList_1735);
		hdlList_097C = handleStorage.getHandles("097C");
		listMap.put(3,hdlList_097C);
		hdlList_203C = handleStorage.getHandles("203C");
		listMap.put(1,hdlList_203C);
		hdlList_001M = handleStorage.getHandles("001M");
		listMap.put(5,hdlList_001M);
		
		this.knwonHandles = new ArrayList<String>();
	}
	
	
	public String getRandomHandle(){
		
		int listID = r_inst.nextInt(5);
		
		ArrayList<String> hdlList = listMap.get(listID);
		int handleID = r_hdl.nextInt(hdlList.size()-1);
		return hdlList.get(handleID);
	}
	
	public void putToKnown(String handle){
		this.knwonHandles.add(handle);
	}
	
	public String getRandomKnownHandle(){
		int handleId = k_hdl.nextInt(knwonHandles.size()-1);
		return knwonHandles.get(handleId);
	}

}
