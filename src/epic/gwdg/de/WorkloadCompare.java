package epic.gwdg.de;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import net.handle.hdllib.HandleRecordsBulk;
import net.handle.hdllib.HandleValue;
import net.handle.hdllib.SingleHandleRecord;
import net.handle.hdllib.Util;

public class WorkloadCompare {
	
		private HandleDataSets handleDataSets;
		private HandleMethods hs;
	 	private String prefix = Config.getInstance().prefix;
	 	private int threadCount =  Config.getInstance().threadcount;
	 	private String hs_seckey = Config.getInstance().hsseckey;
	 	private String userHandle = Config.getInstance().userhandle;
	 	private int index = Config.getInstance().index;
	 	private int bulkSizes [] = Config.getInstance().bulkSizes;
	 	private int roundCount = Config.getInstance().roundcount;
	 	
	    int warmupbulkSize = 1000;	
		
	
		public WorkloadCompare() throws InterruptedException{
		
			System.out.println("WorkLoadCompare Constructor");
		handleDataSets = HandleDataSets.getInstance();
		hs = new HandleMethods(hs_seckey,userHandle,index);
		
		ArrayList<SingleHandleRecord> warmUphandleRecords = handleDataSets.getHandleRecords(warmupbulkSize);
		HandleRecordsBulk warmUpRecordsBulk = getWarmUpHandleRecordsBulk(warmUphandleRecords);
		System.out.println("WorkLoadCompare: CreateWarmUpBulk");
		/* WARM UP THE SERVICE*/
		hs.createHandlesBulk(warmUpRecordsBulk);

		System.out.println("WARM UP WITH BULKSIZE OF 1000 DONE !!");
		
		
		for(int bz = 0; bz < bulkSizes.length; bz++){
			int bulkSize = bulkSizes[bz];
			 for(int rr = 0; rr < roundCount; rr++){
				 
				 
				 ArrayList<SingleHandleRecord> handleRecords = handleDataSets.getHandleRecords(bulkSize);
				 HandleRecordsBulk recordsBulk = getHandleRecordsBulk(handleRecords,rr);										// for ONE BIG BULK
				 ArrayList<HandleRecordsBulk> recordsBulkList =  getRecordsBulkForThreads(handleRecords,rr);				// for parallel processing
				 HandleRecordsBulk recordsBulkForSingleProcesing = getHandleRecordsBulkForSingleThread(handleRecords,rr);	// for single processing
				 
				 	
				/*######################################################################################*/
				/*                           SINGLE THREADED WOHLE BULK									*/
				/*--------------------------------------------------------------------------------------*/
				/*Register Bulk single threaded*/
				 ArrayList<Long> individual_times = new ArrayList<Long>();
				long start_singleBulk = System.currentTimeMillis();

				for(int ss = 0;ss < recordsBulkForSingleProcesing.size();ss++){
					SingleHandleRecord singleHandleRecord = recordsBulkForSingleProcesing.getHandleRecord(ss);
					String handleStr = Util.decodeString(singleHandleRecord.handle);
					HandleValue [] values  = singleHandleRecord.values;
					
					// measure single request
					long start_sss = System.currentTimeMillis();
					hs.createHandle(handleStr, values);
					long elapsed_sss = System.currentTimeMillis() - start_sss;
					//
					
					individual_times.add(elapsed_sss);
				}
								
				long elapsedSingleBulk = System.currentTimeMillis()-  start_singleBulk;	
				
				StringBuilder strBuilder = new StringBuilder();
				strBuilder.append("s,"+rr+",");
				strBuilder.append(bulkSize+",");
				for(int i=0;i<individual_times.size();i++){
					strBuilder.append(individual_times.get(i));
					strBuilder.append(",");
				}
				strBuilder.append(elapsedSingleBulk);
				writeToFile(strBuilder.toString());
				
				float tp_single = bulkSize*1000/elapsedSingleBulk;
				System.out.println(bulkSize+" handles = "+elapsedSingleBulk+"ms => "+tp_single+"handles/sec");
				System.out.println(">>>>>> (ST) BZ = "+bz+" RoundID = "+rr);
				
				/*------------------------------------------------------------------------------------*/
				/*######################################################################################*/
				
				/*######################################################################################*/
				/* 							MULTITHREADED WOHLE BULK                                    */
				/*--------------------------------------------------------------------------------------*/
				/* A Threadpool */
				ExecutorService executor = Executors.newFixedThreadPool(threadCount);
				
				/*execute the individual Runnables*/
				long startPool = System.currentTimeMillis();
				ArrayList<ArrayList<Long>> individual_thread_times = new ArrayList<ArrayList<Long>>();
				 
			    for(int i=0;i<threadCount;i++){
				    
					ArrayList<Long> rt_times_of_thread = new ArrayList<Long>();
					individual_thread_times.add(rt_times_of_thread);
					HandleRecordsBulk t_recordsBulk = recordsBulkList.get(i);
					CreateMultipleRecordsRunnable createThreaded = new CreateMultipleRecordsRunnable(t_recordsBulk,hs);
			
					createThreaded.individual_times = rt_times_of_thread;
					/*execute the worker which process its dedicated list of HandleRecords*/
					executor.execute(createThreaded);
				}
				
				executor.shutdown();
				executor.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
				long elapsedPool = System.currentTimeMillis() - startPool;
				
				StringBuilder strBuilderP = new StringBuilder();
				strBuilderP.append("p,"+rr+","+bulkSize+",");
				for(int jj = 0;jj<individual_thread_times.size();jj++){
					ArrayList<Long> current_thread_times = individual_thread_times.get(jj);
					for (int ii=0;ii<current_thread_times.size();ii++){
						strBuilderP.append(current_thread_times.get(ii)+",");
					}
				}
				strBuilderP.append(elapsedPool);
				writeToFile(strBuilderP.toString());
				
				float tp1 = bulkSize*1000/elapsedPool;
				System.out.println(bulkSize+" handles = "+elapsedPool+"ms => "+tp1+"handles/sec");
				System.out.println(">>>>>> (PT) BZ = "+bz+" RoundID = "+rr);

				
	            System.out.println("Finished all threads");
	            /*------------------------------------------------------------------------------------*/
	            /*######################################################################################*/
				
				
	            /*######################################################################################*/
	            /* 							ONE BULKED REQUEST WOHLE BULK								*/
				/*--------------------------------------------------------------------------------------*/
				/* process a single Bulk Request (isolated) */
				long startBulk = System.currentTimeMillis();
				hs.createHandlesBulk(recordsBulk);
				long elapsedBulk = System.currentTimeMillis() - startBulk;
				writeToFile("b,"+rr+","+bulkSize+","+elapsedBulk);
				
				float tp = bulkSize*1000/elapsedBulk;
				System.out.println(bulkSize+" handles = "+elapsedBulk+"ms => "+tp+"handles/sec");
				System.out.println(">>>>>> (B) BZ = "+bz+" RoundID = "+rr);
				
				System.out.println("SINGLE BULK PROCESSED!");
				/*------------------------------------------------------------------------------------*/
				/*######################################################################################*/
				float su = elapsedPool / elapsedBulk;
				System.out.println("Speedup (Multithreaded / Bulked) = "+su);
				
			}
			
			
		}
		
	}

	public HandleRecordsBulk getWarmUpHandleRecordsBulk(ArrayList<SingleHandleRecord> handleRecords){
			
		HandleRecordsBulk recordsBulk = new HandleRecordsBulk(prefix);
		for(int i=0;i<handleRecords.size();i++){
				UUID uid = UUID.randomUUID();
				SingleHandleRecord singleHandleRecord = handleRecords.get(i);
				recordsBulk.add(uid.toString()+":w", singleHandleRecord.values);
		}
		return recordsBulk;		
	}
		
	public HandleRecordsBulk getHandleRecordsBulk(ArrayList<SingleHandleRecord> handleRecords,int rid){
		
		HandleRecordsBulk recordsBulk = new HandleRecordsBulk(prefix);
		int bz = handleRecords.size();
		for(int i=0;i<handleRecords.size();i++){
			UUID uid = UUID.randomUUID();
			SingleHandleRecord singleHandleRecord = handleRecords.get(i);
			recordsBulk.add(uid.toString()+":b:"+String.valueOf(rid)+":"+String.valueOf(bz), singleHandleRecord.values);
		}
		return recordsBulk;
		
	}
	
	public HandleRecordsBulk getHandleRecordsBulkForSingleThread(ArrayList<SingleHandleRecord> handleRecords, int roundId){
		
		HandleRecordsBulk recordsBulk = new HandleRecordsBulk(prefix);
		for(int i=0;i<handleRecords.size();i++){
			UUID uid = UUID.randomUUID();
			SingleHandleRecord singleHandleRecord = handleRecords.get(i);
			String suffix =uid.toString()+":s:"+String.valueOf(roundId)+":"+String.valueOf(handleRecords.size());
			recordsBulk.add(suffix,singleHandleRecord.values);
		}
		return recordsBulk;
		
	}
	
	
	
	public ArrayList<HandleRecordsBulk> getRecordsBulkForThreads(ArrayList<SingleHandleRecord> handleRecords,int roundId){
		
		ArrayList<HandleRecordsBulk> recordsBulksList = new ArrayList<HandleRecordsBulk>();
		
		int recordsBulkSize = handleRecords.size();
		int bulkSize = recordsBulkSize / threadCount;

		int recordIdx = 0;

		HandleRecordsBulk splitted_recordsBulk = new HandleRecordsBulk(prefix);
		
		while(recordIdx < handleRecords.size()-1){
			
			SingleHandleRecord handleRecord = handleRecords.get(recordIdx);
			UUID uid = UUID.randomUUID();
			String suffix  = uid.toString()+":p:"+String.valueOf(roundId)+":"+String.valueOf(recordsBulkSize);
			HandleValue [] values = handleRecord.values;
			splitted_recordsBulk.add(suffix, values);
			if (splitted_recordsBulk.size() == bulkSize){
				recordsBulksList.add(splitted_recordsBulk);
				splitted_recordsBulk = new HandleRecordsBulk(prefix);
			}
			
	
			recordIdx++;
			
		
		}
		
		recordsBulksList.add(splitted_recordsBulk);
		return recordsBulksList;
	}
	
	public synchronized void writeToFile(String s) {

		FileWriter output = null;
	  	try {
	  		output = new FileWriter("/tmp/hslog",true);
	  		BufferedWriter writer = new BufferedWriter(output);
	  		
	  		
	  		writer.write(s+"\n");
	  		writer.close();

	  	} catch (Exception e) {
	  	System.out.println("filewritererror");
	    throw new RuntimeException(e);
	  } finally {
	    if (output != null) {
	      try {
	        output.close();
	      } catch (IOException e) {
	        // Ignore issues during closing
	      }
	    }
	  }
	}
	
}
