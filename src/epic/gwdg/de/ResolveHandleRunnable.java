package epic.gwdg.de;

import net.handle.hdllib.SiteInfo;

public class ResolveHandleRunnable implements Runnable {


	
	private HandleMethods handleMethods;
	private long endTime;
	private long interval;
	private SiteInfo siteInfo;
	
	public ResolveHandleRunnable(HandleMethods handleMethods, long endTime, long interval){
		this.handleMethods = handleMethods;
		this.endTime = endTime;
		this.interval = interval;

	}
	
	
	public void setSiteInfo(SiteInfo siteInfo){
		this.siteInfo = siteInfo;
	}
	
	@Override
	public void run() {
		
		
		while( (System.currentTimeMillis()<endTime) ){
			
			String handle = HandleLists.getInstance().getRandomKnownHandle();
			int resCode = handleMethods.readHandleFromSite(handle,this.siteInfo);
			System.out.println("RESOLVE HANDLE RC = "+resCode);
			try {
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			
			
		}
		
	}

}
