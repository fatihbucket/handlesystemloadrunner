package epic.gwdg.de;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {
	
	public int privateKeyAuthFlag;
	public String privateKeyFile;
	public String prefix;
	public String userhandle;
	public int index;
	public String hsseckey;
	public int roundcount;
	public int threadcount;
	public String runtype;
	public int [] bulkSizes;
	public String dbUrl;
	public String dbUser;
	public String dbPasswd;
	public boolean trace;
	
	public int sqlite;
	public int runTime;
	public long resolutionInterval;
	public long creationInterval;
	public long bulkInterval;
	public long bulkSingleInterval;
	
	public int internalThreadPoolSize;
	public int fixedBulkSize;
	
	private static Config instance  = null;
	
	public static void createInstance(String configFilename) {
		instance = new Config(configFilename);
	}
    
	public static Config getInstance(){
		return instance;
	}
	
	private Config(String config_filename){
		
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream(config_filename);

			// load a properties file
			prop.load(input);

			this.sqlite = Integer.parseInt(prop.getProperty("sqlite"));
			int int_trace = Integer.parseInt(prop.getProperty("hs_trace"));
			if (int_trace == 1){
				trace = true;
			}
			else{ 
				trace = false;
			
			}
			this.privateKeyAuthFlag = Integer.parseInt(prop.getProperty("private_key_auth_flag"));
			this.privateKeyFile = (String) prop.getProperty("private_key_file");
			this.prefix = (String) prop.getProperty("prefix");
			this.userhandle = (String) prop.getProperty("userhandle");
			this.index = Integer.parseInt(prop.getProperty("index"));
			this.hsseckey = (String) prop.getProperty("hsseckey");
			this.runtype = (String) prop.getProperty("runtype");
			this.roundcount = Integer.parseInt(prop.getProperty("roundcount"));
			this.threadcount = Integer.parseInt(prop.getProperty("threadcount"));
			String bulkSzes_str  = prop.getProperty("bulksizes");
			String splt_bzes [] = bulkSzes_str.split(",");
			int bz_len = splt_bzes.length;
			this.bulkSizes = new int[bz_len];
			for(int i= 0; i< bz_len;i++)
				this.bulkSizes[i] = Integer.parseInt(splt_bzes[i]);
			this.dbUrl =  (String) prop.getProperty("dburl");
			this.dbUser =  (String) prop.getProperty("dbuser");
			this.dbPasswd =  (String) prop.getProperty("dbpasswd");
			
			this.runTime = Integer.valueOf(prop.getProperty("runtime"));
			this.resolutionInterval = Long.valueOf(prop.getProperty("resolution_interval"));
			this.creationInterval = Long.valueOf(prop.getProperty("creation_interval"));
			this.bulkInterval = Long.valueOf(prop.getProperty("bulk_interval"));
			this.bulkSingleInterval = Long.valueOf(prop.getProperty("bulk_single_interval"));
			this.fixedBulkSize = Integer.parseInt(prop.getProperty("fixed_bulk_size"));
			this.internalThreadPoolSize = Integer.parseInt(prop.getProperty("internal_thread_pool_size"));
			

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	

	
}
