package epic.gwdg.de;


import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import net.handle.hdllib.HandleRecordsBulk;
import net.handle.hdllib.SingleHandleRecord;
import net.handle.hdllib.Util;

public class CreateSingleRunnable3 implements Runnable {

	private HandleMethods handleMethods;
	private ArrayList<SingleHandleRecord>  singleHandleRecordList;
	private Random handleItem = new Random();
	private String tid;
	private long endTs;
	private String thePrefix;
	
	public CreateSingleRunnable3(ArrayList<SingleHandleRecord> singleHandleRecordList, HandleMethods handleMethods, int tid,long endTs,String prefix){
		this.singleHandleRecordList = singleHandleRecordList;
		this.handleMethods = handleMethods;
		this.tid = String.valueOf(tid);
		this.endTs = endTs;
		this.thePrefix = prefix;

		
	}

	
	
	@Override
	public void run() {
		
		while(System.currentTimeMillis()<=this.endTs) {
			
			int rndomHandle = handleItem.nextInt(singleHandleRecordList.size());
			UUID suffix = UUID.randomUUID();
			SingleHandleRecord handleRecord = singleHandleRecordList.get(rndomHandle);
			
			
		    String theHandle = thePrefix+"/"+suffix.toString()+":p:"+this.tid+":777";
		    
			int resCode = handleMethods.createHandle3(Util.encodeString(theHandle), handleRecord.values,tid);
			if (resCode != 1){
				System.out.println("ERROR for "+theHandle);
			}
			
		}
		
		
			
	}

}
