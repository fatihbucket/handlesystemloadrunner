package epic.gwdg.de;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbcp.BasicDataSource;

import net.cnri.util.StringUtils;
import net.handle.hdllib.HandleValue;
import net.handle.hdllib.Util;
import net.handle.hdllib.ValueReference;

public class SQLiteMethods implements HandleStorage {
	
	Connection connection;
	//private String GET_HDL_STMT_DATA = "select * from handles where handle=? LIMIT ? OFFSET ?";
	private String GET_HDL_STMT_DATA = "select * from handles where handle=?";
    private String GET_HDL_STMT_HANDLES = "select * from handles where type=? and data=?";

	
    private PreparedStatement preparedStatementHandles = null;
	private PreparedStatement preparedStatementData = null;
	
	private String databaseName = "jdbc:sqlite:handles.db";
	
	
	
	
	public SQLiteMethods(){
			
		System.out.println("SQLITE Constructor");
		
		
	}

	public synchronized void establishConnection(){
		
		System.out.println("SQLITE: Open Database");
		try{
			
		  Class.forName("org.sqlite.JDBC");
		 
	      connection = DriverManager.getConnection(databaseName);
	   
	      
	    } catch ( Exception e ) {
        System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        System.exit(0);
		}
		
	}
	
	
	public synchronized ArrayList<String> getHandles(String instCode){
		
		ArrayList<String> hdlList = new ArrayList<String>();
		try {
		
			preparedStatementHandles = connection.prepareStatement(GET_HDL_STMT_HANDLES);
			preparedStatementHandles.setBytes(1, Util.encodeString("CREATOR"));
			preparedStatementHandles.setBytes(2,Util.encodeString(instCode));
			
			ResultSet rs = preparedStatementHandles.executeQuery();
			 
			 while(rs.next()){
				 String hdl =  rs.getString(1);
				 hdlList.add(hdl);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   
		return hdlList;
	}
	
	public synchronized List<HandleValue> getHandleRecord(String handle){
		
		List<HandleValue> values = new ArrayList<HandleValue>();

		try {
			
			preparedStatementData = connection.prepareStatement(GET_HDL_STMT_DATA);
			preparedStatementData.setString(1,handle);
			ResultSet rs = preparedStatementData.executeQuery();
			 
			 while(rs.next()){
				 HandleValue value = new HandleValue();
				 
				  value.setIndex(rs.getInt(2));
				  value.setType(rs.getBytes(3));
				  value.setData(rs.getBytes(4));
	              value.setTTLType(rs.getByte(5));
	              value.setTTL(rs.getInt(6));
	              value.setTimestamp(rs.getInt(7));
	                String referencesStr = getDecodedStringFromResults(rs, 8);

	                // parse references...
	                String references[] = StringUtils.split(referencesStr, '\t');
	                if (references != null && referencesStr.length() > 0 && references.length > 0) {
	                    ValueReference valReferences[] = new ValueReference[references.length];
	                    for (int i = 0; i < references.length; i++) {
	                        valReferences[i] = new ValueReference();
	                        int colIdx = references[i].indexOf(':');
	                        try {
	                            valReferences[i].index = Integer.parseInt(references[i].substring(0, colIdx));
	                        }
	                        catch (Exception t) {}
	                        valReferences[i].handle = Util.encodeString(StringUtils.decode(references[i].substring(colIdx + 1)));
	                    }
	                    value.setReferences(valReferences);
	                }

	                
	                boolean admin_can_read = false;
	                boolean admin_can_write = false;
	                boolean public_can_read = false;
	                boolean public_can_write = false;
	                
	                if (rs.getInt(9) == 1)
	                	admin_can_read = true;
	                if (rs.getInt(10) == 1)
	                	admin_can_write = true;
	                if (rs.getInt(11) == 1)
	                	public_can_read = true;             
	                if (rs.getInt(12) == 1)
	                	public_can_write = true;
	   
	                
	                value.setAdminCanRead(admin_can_read);
	                value.setAdminCanWrite(admin_can_write);
	                value.setAnyoneCanRead(public_can_read);
	                value.setAnyoneCanWrite(public_can_write);
	                values.add(value);
				 
				 
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			try {
				preparedStatementData.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	   
		return values;
	}
	
	
	  private final String getDecodedStringFromResults(ResultSet results, int i) throws SQLException {
		    String s = results.getString(i);
	        return s == null ? "" : decodeString(s);
	    }

	    private static final String decodeString(String str) {
	        int len = str.length();
	        StringBuffer sb = new StringBuffer(len);
	        for (int i = 0; i < len; i++) {
	            char ch = str.charAt(i);
	            if (ch == '%' && i < len - 4) {
	                char encCh1 = str.charAt(++i);
	                char encCh2 = str.charAt(++i);
	                char encCh3 = str.charAt(++i);
	                char encCh4 = str.charAt(++i);
	                sb.append(decodeChar(encCh1, encCh2, encCh3, encCh4));
	            } else {
	                sb.append(ch);
	            }
	        }
	        return sb.toString();
	    }
	    
	    private static final char decodeChar(char ch1, char ch2, char ch3, char ch4) {
	        int ich1 = (ch1 >= 'a') ? (ch1 - 'a') + 10 : ((ch1 >= 'A') ? (ch1 - 'A') + 10 : (ch1 - '0'));
	        int ich2 = (ch2 >= 'a') ? (ch2 - 'a') + 10 : ((ch2 >= 'A') ? (ch2 - 'A') + 10 : (ch2 - '0'));
	        int ich3 = (ch3 >= 'a') ? (ch3 - 'a') + 10 : ((ch3 >= 'A') ? (ch3 - 'A') + 10 : (ch3 - '0'));
	        int ich4 = (ch4 >= 'a') ? (ch4 - 'a') + 10 : ((ch4 >= 'A') ? (ch4 - 'A') + 10 : (ch4 - '0'));
	        return (char) ((ich1 << 12) | (ich2 << 8) | (ich3 << 4) | (ich4));
	    }
}
