package epic.gwdg.de;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.LoadingCache;

import net.handle.hdllib.SingleHandleRecord;

public class HandleDataSets {
	
	private LoadingCache<String, SingleHandleRecord> handleCache  = null;
	private static HandleDataSets instance = null;
	
	public static HandleDataSets getInstance(){
		 if(instance == null) {
	          instance = new HandleDataSets();
	       }
	       return instance;
	}
	
	private HandleDataSets(){
		this.handleCache = 
		         CacheBuilder.newBuilder()
		            .maximumSize(10000) // maximum 5000 records can be cached
		            .expireAfterAccess(60, TimeUnit.MINUTES) // cache will expire after 60 minutes of access
		            .build(new HandleCacheLoader()); // build the cacheloader
		
	}
	
	public ArrayList<SingleHandleRecord> getHandleRecords(int bulkSize){
		
		ArrayList<SingleHandleRecord> handleRecords = new ArrayList<SingleHandleRecord>();
		for(int i=0;i<bulkSize;i++){
			
			String rdnmHandleStr = HandleLists.getInstance().getRandomHandle();
			try {
				//System.out.println("get random handle = "+rdnmHandleStr);
 				handleRecords.add(this.handleCache.get(rdnmHandleStr));
			} catch (ExecutionException e) {
				
				e.printStackTrace();
			}
		}
		return handleRecords;
	}
	
	public SingleHandleRecord getHandleRecord(){
		
		SingleHandleRecord ret = null;
		try {
			String rdnmHandleStr = HandleLists.getInstance().getRandomHandle();
			
			ret = this.handleCache.get(rdnmHandleStr);
		} catch (ExecutionException e) {
			
			e.printStackTrace();
		}

		return ret;
	}
	
	
}
