package epic.gwdg.de;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.PrivateKey;

import net.handle.hdllib.AbstractResponse;
import net.handle.hdllib.AuthenticationInfo;
import net.handle.hdllib.CreateHandleRequest;
import net.handle.hdllib.CreateHandlesBulkRequest;
import net.handle.hdllib.DeleteHandleRequest;
import net.handle.hdllib.Encoder;
import net.handle.hdllib.HandleException;
import net.handle.hdllib.HandleRecordForBulk;
import net.handle.hdllib.HandleRecordsBulk;
import net.handle.hdllib.HandleResolver;
import net.handle.hdllib.HandleValue;
import net.handle.hdllib.PublicKeyAuthenticationInfo;
import net.handle.hdllib.ResolutionRequest;
import net.handle.hdllib.RetrieveTxnRequest;
import net.handle.hdllib.SecretKeyAuthenticationInfo;
import net.handle.hdllib.ServerInfo;
import net.handle.hdllib.SingleHandleRecord;
import net.handle.hdllib.SiteInfo;
import net.handle.hdllib.Util;

public class HandleMethods {
	
	private HandleResolver resolver = null;
	private AuthenticationInfo authInfo;
	
	
	
	
	public HandleMethods(String hs_seckey, String userHandle, int index){
		
		
		if (Config.getInstance().privateKeyAuthFlag == 1){
			System.out.println("HS PUBKEY AUTH");
			this.authInfo = getAuthInfoPub(Config.getInstance().privateKeyFile,userHandle,index);
			
		}
		else{
			System.out.println("HS SECKEY AUTH");
			this.authInfo = getAuthInfoSec(hs_seckey,userHandle,index);
		}
		//this.authInfo = getAuthInfoSec(hs_seckey,userHandle,index);
		this.resolver = new HandleResolver();
	
		this.resolver.traceMessages = Config.getInstance().trace;
		
	
			
	}
	
	

	
	public int createHandle(byte [] handle,HandleValue [] values){
		
		return createHandle(Util.decodeString(handle),values);
	}
	
	public int createHandle(String handle,HandleValue [] values){
		
		CreateHandleRequest createReq = new CreateHandleRequest( Util.encodeString(handle),values, this.authInfo);
		
		AbstractResponse response = null;
		int resCode = -1;
		
		try {
			response = resolver.processRequest(createReq);
			resCode = response.responseCode;
			if (resCode != 1)
				System.out.println("resCode == "+String.valueOf(resCode));
		} catch (HandleException e) {
			System.out.println("Excpetion in single Create("+Util.encodeString(handle)+") = "+ e.getMessage());
			e.printStackTrace();
		}
		
		
		return resCode;
	}
	
	public int createHandle3(byte [] handle,HandleValue [] values,String tid){
		
		CreateHandleRequest createReq = new CreateHandleRequest(handle,values, this.authInfo);
		
		AbstractResponse response = null;
		int resCode = -1;
		
		try {
			response = resolver.processRequest(createReq);
			resCode = response.responseCode;
			if (resCode != 1)
				System.out.println("resCode == "+String.valueOf(resCode));
		} catch (HandleException e) {
			System.out.println(e.getMessage()+" ## for threadCount="+tid);
						
		}
		
		return resCode;
	}
	
	
	
	public SiteInfo getPrimarySite(String naHandle){
		
		SiteInfo thePrimary = null;
		
		try {
			SiteInfo siteInfo [] = resolver.findLocalSitesForNA(Util.encodeString(naHandle));
			for(int i=0;i<siteInfo.length;i++){
				
				if (siteInfo[i].isPrimary)
					thePrimary = siteInfo[i];
					
				
			}
		} catch (HandleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return thePrimary;
	}
	
	public int readHandleFromSite(String handle,SiteInfo siteInfo){
		
		ResolutionRequest readReq = new ResolutionRequest(Util.encodeString(handle), null, null, authInfo);
		
		
		
		AbstractResponse response = null;
		int resCode = -1;
		
		try {
			response = resolver.sendRequestToSite(readReq, siteInfo);
			resCode = response.responseCode;
		} catch (HandleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return resCode;
	}
	
	
	
	public int readHandle(String handle){
		
		ResolutionRequest readReq = new ResolutionRequest(Util.encodeString(handle), null, null, authInfo);
		
		
		
		AbstractResponse response = null;
		int resCode = -1;
		
		try {
			response = resolver.processRequest(readReq);
			resCode = response.responseCode;
		} catch (HandleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return resCode;
	}
	
	public int updateHandle(byte [] handle,HandleValue [] values){
		
		return updateHandle(Util.decodeString(handle),values);
	}
	
	public int updateHandle(String handle, HandleValue [] values){
		
		CreateHandleRequest createReq = new CreateHandleRequest( Util.encodeString(handle),values, authInfo);
		createReq.overwriteWhenExists = true;
		
		AbstractResponse response = null;
		int resCode = -1;
		
		try {
			response = resolver.processRequest(createReq);
			resCode = response.responseCode;
		} catch (HandleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return resCode;
	}

	public int deleteHandle(String handle){
		
		DeleteHandleRequest deleteReq = new DeleteHandleRequest( Util.encodeString(handle), authInfo);

		
		AbstractResponse response = null;
		int resCode = -1;
		
		try {
			response = resolver.processRequest(deleteReq);
			resCode = response.responseCode;
		} catch (HandleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return resCode;
	}
	public int recordSize(HandleRecordsBulk recordsBulk){
		
		int  rsize = 0;
		try {
			CreateHandlesBulkRequest bulkReq = new CreateHandlesBulkRequest(recordsBulk,this.authInfo);
			byte [] bulkMsg = Encoder.encodeCreateHandlesBulkRequest(bulkReq);
			rsize = bulkMsg.length;
		} catch (HandleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rsize;
		
	}
	
	public int recordSizeSingleHandleRecords(HandleRecordsBulk recordsBulk){
		
		int  rsize = 0;
		try {
			for(int i=0;i<recordsBulk.size();i++){
				
				SingleHandleRecord handleRecord = recordsBulk.getHandleRecord(i);
				CreateHandleRequest singleReq = new CreateHandleRequest(handleRecord.handle,handleRecord.values,this.authInfo);
				byte [] singleMsg = Encoder.encodeCreateHandleRequest(singleReq);
				rsize += singleMsg.length;
			}
			
			
		} catch (HandleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rsize;
		
	}
	
	public int createHandlesBulk(HandleRecordsBulk recordsBulk){
		int resCode = -1;
		try {
			
			CreateHandlesBulkRequest bulkReq = new CreateHandlesBulkRequest(recordsBulk,this.authInfo);
		
			System.out.println("BULK HANLDE ="+Util.decodeString(bulkReq.handle));
			AbstractResponse response = resolver.processRequest(bulkReq);
			resCode = response.responseCode;
			
		
		} catch (HandleException e) {
			System.out.println("error Message for BulkRequest = "+e.getMessage().toString());
			e.printStackTrace();
		}
		
		return resCode;
	}

	public int updateHandlesBulk(HandleRecordsBulk recordsBulk){
		int resCode = -1;
		try {
			System.out.println("UPDATING the Bulk ");
			CreateHandlesBulkRequest bulkReq = new CreateHandlesBulkRequest(recordsBulk,this.authInfo);
			bulkReq.overwriteWhenExists = true;
				
			AbstractResponse response = resolver.processRequest(bulkReq);
			resCode = response.responseCode;
			
		
		} catch (HandleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return resCode;
	}
	

	private AuthenticationInfo getAuthInfoSec(String hs_seckey,String userHandle,int index){
		
		AuthenticationInfo auth = null;

		try {
			auth = new SecretKeyAuthenticationInfo(Util.encodeString(userHandle),index,Util.encodeString(hs_seckey),true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return auth;
		}
	
	
	private AuthenticationInfo getAuthInfoPub(String privateKeyFile,String userHandle,int idx){
		
		
		int index = idx;
		String passphrase = null;
		File keyFile = new File(privateKeyFile);
		
		try{
			byte[] rawKey = new byte[(int) keyFile.length()];
			InputStream in = new FileInputStream(keyFile);
			int n =0; int r=0;
        while(n<rawKey.length && (r=in.read(rawKey, n, rawKey.length-n))>0)
          n +=r;
        in.close();
        byte[] keyBytes = passphrase==null ? Util.decrypt(rawKey,new byte[0]) : Util.decrypt(rawKey, Util.encodeString(passphrase));

        PrivateKey privateKey = Util.getPrivateKeyFromBytes(keyBytes, 0);
        PublicKeyAuthenticationInfo
          pubkeyAuthInfo = new PublicKeyAuthenticationInfo(Util.encodeString(userHandle),
                                                          index,privateKey);
        
			return pubkeyAuthInfo;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	

		return null;
	}
	
}
