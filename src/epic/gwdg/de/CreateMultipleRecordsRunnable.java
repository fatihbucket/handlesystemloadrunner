package epic.gwdg.de;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import net.handle.hdllib.HandleRecordsBulk;
import net.handle.hdllib.SingleHandleRecord;

public class CreateMultipleRecordsRunnable implements Runnable {

	private HandleMethods handleMethods;
	private HandleRecordsBulk recordsBulk;

	public ArrayList<Long> individual_times;
	
	public CreateMultipleRecordsRunnable(HandleRecordsBulk recordsBulk,HandleMethods handleMethods){
		this.recordsBulk =  recordsBulk;
		this.handleMethods = handleMethods;


		
	}
	
	
	@Override
	public void run() {
		
		for(int i=0;i<recordsBulk.size();i++){
			SingleHandleRecord handleRecord = recordsBulk.getHandleRecord(i);
			
			long start = System.currentTimeMillis();
			handleMethods.createHandle(handleRecord.handle, handleRecord.values);
			long elapsed =  System.currentTimeMillis() - start;
			
			individual_times.add(elapsed);
			
		}
		
		
	
	}

	
	public synchronized void writeToFile(String s) {

		FileWriter output = null;
	  	try {
	  		output = new FileWriter("/tmp/hslog",true);
	  		BufferedWriter writer = new BufferedWriter(output);
	  		
	  		
	  		writer.write(s+"\n");
	  		writer.close();

	  	} catch (Exception e) {
	  	System.out.println("filewritererror");
	    throw new RuntimeException(e);
	  } finally {
	    if (output != null) {
	      try {
	        output.close();
	      } catch (IOException e) {
	        // Ignore issues during closing
	      }
	    }
	  }
	}
	
	
}
