package epic.gwdg.de;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import net.handle.hdllib.HandleRecordsBulk;
import net.handle.hdllib.HandleValue;
import net.handle.hdllib.SingleHandleRecord;
import net.handle.hdllib.Util;


public class CreateBulkSingleThreads implements Runnable {

	private int maxBulkSize = 10000;
	private int minBulkSize = 1000;
	private int handlePacketSize = 10;
	
	
	public boolean the_methodSelection = true;
   
	
	ExecutorService executor;
	
	private Random methodRndm = new Random();
	private Random bulkSz = new Random();
	private Random sleepInt = new Random();
	
	
	private HandleMethods handleMethods;
	private long endTime;
	private long interval;

	
	public CreateBulkSingleThreads(HandleMethods handleMethods,long endTime,long interval){
	
		this.handleMethods = handleMethods;
		this.endTime = endTime;
		this.interval = interval;
	}
	
	
	@Override
	public void run() {
	
		while( (System.currentTimeMillis()<endTime) ){
			
			long tid = Thread.currentThread().getId();
			
			System.out.println("TID="+tid+"MIXED WORKLOAD Production ");
			
			int methodSelection = methodRndm.nextInt(3);
		
			if(the_methodSelection){
				methodSelection = 1;
			}
			else{
				methodSelection = 3;
			}
		
			System.out.println("MIXTURE DECISSION");
			
			if (methodSelection < 2){
				int bz =  bulkSz.nextInt((maxBulkSize - minBulkSize) + 1) + minBulkSize;
				
				ArrayList<SingleHandleRecord> handleRecords = HandleDataSets.getInstance().getHandleRecords(bz);
				HandleRecordsBulk recordsBulk = getHandleRecordsBulk(handleRecords);
				BulkIt(recordsBulk);
			}
			else{
				ArrayList<SingleHandleRecord> handleRecords = HandleDataSets.getInstance().getHandleRecords(handlePacketSize);
				ArrayList<HandleRecordsBulk> recordsBulkList =  getRecordsBulkForThreads(handleRecords);		
				ParallelIt(recordsBulkList);
			}

			try {
				int high = (int) interval;
			    int low = 5000;
				int rndm_interval = sleepInt.nextInt(high - low) + low;
			
				Thread.sleep(rndm_interval);
			} catch (InterruptedException e) {
				System.out.println("ERROR IN MIXTURE DELAY");
				e.printStackTrace();
			} 
		}
	}
	
	
	public void BulkIt(HandleRecordsBulk handleRecordsBulk){
				
		CreateBulkRunnable bulkThread = new CreateBulkRunnable(handleMethods,handleRecordsBulk);
		bulkThread.runZ();
		
	}
	
	public void ParallelIt(ArrayList<HandleRecordsBulk> recordsBulkList ){
		
		
		executor = Executors.newFixedThreadPool(Config.getInstance().internalThreadPoolSize);
		for(int i=0;i<Config.getInstance().internalThreadPoolSize;i++){
			
			HandleRecordsBulk t_recordsBulk = recordsBulkList.get(i);
			CreateSingleRunnable2 createSingle = new CreateSingleRunnable2(t_recordsBulk,handleMethods);
			/*execute the worker which process its dedicated list of HandleRecords*/
			executor.execute(createSingle);
		}
		
		executor.shutdown();
		try {
			executor.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	

	public HandleRecordsBulk getHandleRecordsBulk(ArrayList<SingleHandleRecord> handleRecords){
		
		HandleRecordsBulk recordsBulk = new HandleRecordsBulk(Config.getInstance().prefix);
		for(int i=0;i<handleRecords.size();i++){
			UUID uid = UUID.randomUUID();
			String suffix  = uid.toString()+":g:"+String.valueOf(handleRecords.size());
			SingleHandleRecord singleHandleRecord = handleRecords.get(i);
			recordsBulk.add(suffix, singleHandleRecord.values);
		}
		return recordsBulk;
		
	}
	
	public ArrayList<HandleRecordsBulk> getRecordsBulkForThreads(ArrayList<SingleHandleRecord> handleRecords){
		
		ArrayList<HandleRecordsBulk> recordsBulksList = new ArrayList<HandleRecordsBulk>();
		
		int recordsBulkSize = handleRecords.size();
		int bulkSize = recordsBulkSize / Config.getInstance().internalThreadPoolSize;

		int recordIdx = 0;

		HandleRecordsBulk splitted_recordsBulk = new HandleRecordsBulk(Config.getInstance().prefix);
		
		while(recordIdx < handleRecords.size()-1){
		
			SingleHandleRecord handleRecord = handleRecords.get(recordIdx);
		
			UUID uid = UUID.randomUUID();
			
			String suffix  = uid.toString()+":m";
		
			HandleValue [] values = handleRecord.values;
		
			splitted_recordsBulk.add(suffix, values);
			
			if (splitted_recordsBulk.size() == bulkSize){
				recordsBulksList.add(splitted_recordsBulk);
				
				splitted_recordsBulk = new HandleRecordsBulk(Config.getInstance().prefix);
			}
		   
			recordIdx++;	
		
		}
		
		recordsBulksList.add(splitted_recordsBulk);
		return recordsBulksList;
	}
}
