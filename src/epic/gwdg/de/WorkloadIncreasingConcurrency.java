package epic.gwdg.de;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import net.handle.hdllib.HandleRecordsBulk;
import net.handle.hdllib.SingleHandleRecord;

public class WorkloadIncreasingConcurrency {
	
	
	private HandleDataSets handleDataSets;
	private HandleMethods hs;
 	private String prefix = Config.getInstance().prefix;
 	private String hs_seckey = Config.getInstance().hsseckey;
 	private String userHandle = Config.getInstance().userhandle;
 	private int index = Config.getInstance().index;
 	private int runTime = Config.getInstance().runTime;
 	
    int warmupbulkSize = 1000;	
    
    public WorkloadIncreasingConcurrency() throws InterruptedException{
    	
    	
    	int [] poolSizes = {1,3,5,7,10,20,50};
    	int threadCount;
    	
		System.out.println("Concurrency Constructor");
		handleDataSets = HandleDataSets.getInstance();
		hs = new HandleMethods(hs_seckey,userHandle,index);
		
		ArrayList<SingleHandleRecord> warmUphandleRecords = handleDataSets.getHandleRecords(warmupbulkSize);
		HandleRecordsBulk warmUpRecordsBulk = getWarmUpHandleRecordsBulk(warmUphandleRecords);
		System.out.println("Concurrency: CreateWarmUpBulk");
		/* WARM UP THE SERVICE*/
		ArrayList<SingleHandleRecord> handleRecords = handleDataSets.getHandleRecords(10000);
		System.out.println("got the basic handle records dataset");
		
		System.out.println("NOW DO A WARM UP BULK");	
		hs.createHandlesBulk(warmUpRecordsBulk);

		 
		System.out.println("WARM UP WITH BULKSIZE OF 1000 DONE !!");
		
		
		for(int psz = 0;psz<poolSizes.length;psz++){
			
			threadCount = poolSizes[psz];
			
			/* A Threadpool */
			ExecutorService executor = Executors.newFixedThreadPool(threadCount);
			long startTs = System.currentTimeMillis();
			long endTs = startTs + this.runTime*1000*60;
			for(int i=0;i<threadCount;i++){
		    
				CreateSingleRunnable3 createSingle = new CreateSingleRunnable3(handleRecords,hs,threadCount,endTs,prefix);
				executor.execute(createSingle);
			}
		
			executor.shutdown();
			executor.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
			
    	    Thread.sleep(1000*60);
		}
    	
    }

    
    
    public HandleRecordsBulk getWarmUpHandleRecordsBulk(ArrayList<SingleHandleRecord> handleRecords){
		
		HandleRecordsBulk recordsBulk = new HandleRecordsBulk(prefix);
		for(int i=0;i<handleRecords.size();i++){
				UUID uid = UUID.randomUUID();
				SingleHandleRecord singleHandleRecord = handleRecords.get(i);
				recordsBulk.add(uid.toString()+":w", singleHandleRecord.values);
		}
		return recordsBulk;		
	}
		
}
