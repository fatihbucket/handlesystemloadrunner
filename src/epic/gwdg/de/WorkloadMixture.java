package epic.gwdg.de;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import net.handle.hdllib.HandleRecordsBulk;
import net.handle.hdllib.SingleHandleRecord;
import net.handle.hdllib.SiteInfo;

public class WorkloadMixture {
	
	
	private HandleDataSets handleDataSets;
	private HandleMethods hs;
	
	ExecutorService executor;
	ResolveHandleRunnable resolutionThread;
	CreateSingleRunnable  creationThread;
	CreateBulkRunnable  creationBulkThread;
	CreateBulkSingleThreads  bulkSingleMixture;
	CreateBulkSingleThreads createSingleMixture;
	
	private String prefix = Config.getInstance().prefix;

 	private String hs_seckey = Config.getInstance().hsseckey;
 	private String userHandle = Config.getInstance().userhandle;
 	private int index = Config.getInstance().index;
 	
 	private int runTimeMinutesToMillis = (Config.getInstance().runTime)*1000*60;
	private long endTime = System.currentTimeMillis() + runTimeMinutesToMillis;
	private int fixedBulkSize = Config.getInstance().fixedBulkSize;
	
	
	private long resolutionInterval = Config.getInstance().resolutionInterval;
	private long creationInterval = Config.getInstance().creationInterval;
	private long bulkInterval = Config.getInstance().bulkInterval;
	private long bulkSingleInterval = Config.getInstance().bulkSingleInterval;
	
 	
	private int warmupbulkSize = 10000;	
	
	public WorkloadMixture(){
		
		System.out.println("Preparing WorkloadMixture to Start....");
		handleDataSets = HandleDataSets.getInstance();
		hs = new HandleMethods(hs_seckey,userHandle,index);
		executor = Executors.newFixedThreadPool(5);
		
		System.out.println("Warming up the system....");
		ArrayList<SingleHandleRecord> warmUphandleRecords = handleDataSets.getHandleRecords(warmupbulkSize);
		HandleRecordsBulk warmUpRecordsBulk = getWarmUpHandleRecordsBulk(warmUphandleRecords);
		
		/* WARM UP THE SERVICE*/
		hs.createHandlesBulk(warmUpRecordsBulk);
		/* WARMUP DONE AND KNWON HANDLES FILLED*/
		
		/*---------REGULAR RESOLUTION THREAD----------*/
		resolutionThread = new ResolveHandleRunnable(hs,endTime,resolutionInterval);
		SiteInfo thePrimarySiteInfo = hs.getPrimarySite("0.NA/"+prefix);
		resolutionThread.setSiteInfo(thePrimarySiteInfo);
		
		/*---------REGULAR CREATION THREAD----------*/
		creationThread = new CreateSingleRunnable(hs,endTime,creationInterval);
		
		/*---------REGULAR BULKING THREAD----------*/
		creationBulkThread = new CreateBulkRunnable(hs,endTime,bulkInterval);
		
		/*---------MIXED BULKING/CREATION THREAD----------*/
		bulkSingleMixture = new CreateBulkSingleThreads(hs,endTime,bulkSingleInterval);
		
		
		/*---------MIXED /CREATION THREAD----------*/
		createSingleMixture = new CreateBulkSingleThreads(hs,endTime,bulkSingleInterval);
		createSingleMixture.the_methodSelection = false;
		
		
		System.out.println("executing Resolution Thread");
		executor.execute(resolutionThread);
		System.out.println("executing Creation Thread");
		executor.execute(creationThread);
		System.out.println("executing Bulk-"+fixedBulkSize+" Thread");
		executor.execute(creationBulkThread);
		System.out.println("executing BulkMixture Thread");
		executor.execute(bulkSingleMixture);
		System.out.println("executing Single-Mixture Thread");
		executor.execute(createSingleMixture);
		
		executor.shutdown();
		try {
			executor.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	
	public HandleRecordsBulk getWarmUpHandleRecordsBulk(ArrayList<SingleHandleRecord> handleRecords){
		
		HandleRecordsBulk recordsBulk = new HandleRecordsBulk(prefix);
		for(int i=0;i<handleRecords.size();i++){
				UUID uid = UUID.randomUUID();
				SingleHandleRecord singleHandleRecord = handleRecords.get(i);
				String suffix = uid.toString()+":w";
				String handle = prefix+"/"+suffix;
				recordsBulk.add(suffix, singleHandleRecord.values);
				HandleLists.getInstance().putToKnown(handle);
				
		}
		return recordsBulk;		
	}
	
	

}
