package epic.gwdg.de;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import net.handle.hdllib.HandleRecordsBulk;
import net.handle.hdllib.SingleHandleRecord;

public class CreateBulkRunnable implements Runnable {

	private Random sleepInt = new Random();

	private HandleMethods handleMethods;
	private HandleRecordsBulk handleRecordsBulk;
	private long endTime;
	private long interval;
	
	
	public CreateBulkRunnable(HandleMethods handleMethods, HandleRecordsBulk handleRecordsBulk){
		this.handleRecordsBulk = handleRecordsBulk;
		this.handleMethods = handleMethods;

	}
	
	
	public CreateBulkRunnable(HandleMethods handleMethods, long endTime,long interval){
		this.handleMethods = handleMethods;
		this.endTime = endTime;
		this.interval = interval;
		
	}
	
	
	public HandleRecordsBulk getHandleRecordsBulk(ArrayList<SingleHandleRecord> handleRecords){
		
		HandleRecordsBulk recordsBulk = new HandleRecordsBulk(Config.getInstance().prefix);
		for(int i=0;i<handleRecords.size();i++){
			UUID uid = UUID.randomUUID();
			String suffix  = uid.toString()+":z:"+String.valueOf(handleRecords.size());
			SingleHandleRecord singleHandleRecord = handleRecords.get(i);
			recordsBulk.add(suffix, singleHandleRecord.values);
		}
		return recordsBulk;
		
	}
	
	
	@Override
	public void run() {
		
		
		while( (System.currentTimeMillis()<endTime) ){
			long tid = Thread.currentThread().getId();

			ArrayList<SingleHandleRecord> handleRecords = HandleDataSets.getInstance().getHandleRecords(Config.getInstance().fixedBulkSize);
			HandleRecordsBulk recordsBulk = getHandleRecordsBulk(handleRecords);
			int resCode = handleMethods.createHandlesBulk(recordsBulk);
			
			System.out.println("TID="+tid+"CREATE HANDLES REGULAR BULK BZ:"+Config.getInstance().fixedBulkSize+"  RC = "+resCode);
			
			
			try {
				int high = (int) interval;
			    int low = 5000;
				int rndm_interval = sleepInt.nextInt(high - low) + low;
				
				Thread.sleep(rndm_interval);
			} catch (InterruptedException e) {
				System.out.println("ERROR IN DELAYING BULK");
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
		
	}

	public void runZ() {
		
		int resCode = handleMethods.createHandlesBulk(handleRecordsBulk);
		System.out.println("CREATE HANDLES BULK BZ: Mixture SIZE ="+handleRecordsBulk.size()+"  RC = "+resCode);
			
		
		
	}

	
	
	
	
}
