package epic.gwdg.de;

import java.util.ArrayList;
import java.util.List;

import net.handle.hdllib.HandleValue;

public interface HandleStorage {
	
	public void establishConnection();
	public  ArrayList<String> getHandles(String instCode);
	public  List<HandleValue> getHandleRecord(String handle);
	

}
